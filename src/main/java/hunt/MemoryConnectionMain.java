package hunt;

import java.sql.*;

public class MemoryConnectionMain {

    public static void execSQL(String sql, Connection conn) throws SQLException {
        System.out.println("Executing:" + sql);
        Statement statement = conn.createStatement();
        statement.execute(sql);
    }

    public static String dump(Connection conn) throws SQLException {
        Statement statement = conn.createStatement();
        statement.execute("SCRIPT");
        ResultSet resultSet = statement.getResultSet();
        StringBuffer sb = new StringBuffer();
        while (!resultSet.isLast()) {
            resultSet.next();
            String string = resultSet.getString(1);
            sb.append('\n');
            sb.append(string);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.
                getConnection("jdbc:h2:mem://example;MODE=MySQL;MVCC=TRUE", "sa", "");
        execSQL(
                "CREATE TABLE PUBLIC.TEST_TABLE1\n" +
                        "(COL1 INTEGER NOT NULL,\n" +
                        "COL2 CHAR(25),\n" +
                        "COL3 VARCHAR(25) NOT NULL,\n" +
                        "COL4 DATE,\n" +
                        "PRIMARY KEY (COL1)) "
                , conn);
        execSQL("CREATE TABLE PUBLIC.TEST_TABLE2\n" +
                "(COL1 INTEGER NOT NULL,\n" +
                "COL2 CHAR(25),\n" +
                "COL3 VARCHAR(25) NOT NULL,\n" +
                "COL4 DATE,\n" +
                "PRIMARY KEY (COL1)) "
                , conn);
        String dump = dump(conn);
        System.err.println(dump);
        Connection conn2 = DriverManager.
                getConnection("jdbc:h2:mem://~/hww;MODE=MySQL;MVCC=TRUE", "sa", "");

        execSQL(dump, conn2);
        conn2.close();
        conn.close();
    }
}